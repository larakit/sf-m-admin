<?php
return;
\Larakit\StaticFiles\Manager::package('larakit/sf-m-admin')
    ->usePackage('larakit/sf-bootstrap')
    ->usePackage('larakit/sf-animate.css')
    ->usePackage('larakit/sf-font-awesome')
    ->setSourceDir('public')
    ->jsPackage('lodash.min.js');
